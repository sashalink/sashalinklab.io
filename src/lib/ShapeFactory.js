import * as THREE from 'three';

export default class ShapeFactory {
  createGeometry({ vertices, faces, material }) {
    const geometry = new THREE.Geometry();

    geometry.vertices.push(...vertices);
    geometry.faces.push(...faces);

    return new THREE.Mesh(geometry, material);
  }

  // for saturn view only - threejs has own method to display rings
  createRing({ radius, color, tube, segments }) {
    const geometry = new THREE.TorusGeometry(radius, tube, ...segments);
    const material = new THREE.MeshBasicMaterial({ color });

    return new THREE.Mesh(geometry, material);
  }

  // for saturn view only
  createSphere({ radius, color, segments }) {
    const geometry = new THREE.SphereGeometry(radius, ...segments);
    const material = new THREE.MeshBasicMaterial({ color });

    return new THREE.Mesh(geometry, material);
  }
}
