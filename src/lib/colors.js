export const CGA_COLOR = {
  // dark
  BLACK: 0x000000,
  BLUE: 0x0000aa,
  GREEN: 0x00aa00,
  CYAN: 0x00aaaa,
  RED: 0xaa0000,
  MAGENTA: 0xaa00aa,
  BROWN: 0xaa5500,
  LIGHT_GRAY: 0xaaaaaa,

  // light
  DARK_GRAY: 0x555555,
  LIGHT_BLUE: 0x5555ff,
  LIGHT_GREEN: 0x55ff55,
  LIGHT_CYAN: 0x55ffff,
  LIGHT_RED: 0xff5555,
  LIGHT_MAGENTA: 0xff55ff,
  YELLOW: 0xffff55,
  WHITE: 0xffffff,
}
