import View from './views/ProjectView';
import './assets/main.css';

{
  const view = new View({ debug: false });

  view.init();
  view.mainLoop();
}
