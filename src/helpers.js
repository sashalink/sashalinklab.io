/**
 * @param {number} from
 * @param {number} to
 * @returns {number}
 */
export function getRandomInRange(from, to) {
  const x = Math.random() * (to - from);
  return x + from;
}

/**
 * @returns {number}
 */
export function getRandomHexColor() {
  return Math.random() * 0xffffff;
}
