import * as THREE from 'three';
import ShapeFactory from '../lib/ShapeFactory';

const COLOR_BACKGROUND = 0x9a9a9a;
const WIDTH = window.innerWidth;
const HEIGHT = window.innerHeight;

const CAMERA_FOV = 75;
const CAMERA_NEAR = 1;
const CAMERA_FAR = 1000;
const CAMERA_RATIO = WIDTH / HEIGHT;

export default class BaseView {
  /**
   * @param {boolean} debug
   */
  constructor({ debug = false } = {}) {
    this.renderer = undefined;
    this.camera = undefined;
    this.scene = undefined;

    this.debug = debug;
    this.factory = new ShapeFactory();
  }

  init({
    background = COLOR_BACKGROUND,
    ratio = CAMERA_RATIO,
    width = WIDTH,
    height = HEIGHT,
  } = {}) {
    this.setupScene(background);
    this.setupCamera(ratio);

    this.addObjects();
    this.initRenderer(width, height);
  }

  setupScene(background) {
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(background);
  }

  setupCamera(ratio) {
    this.camera = new THREE.PerspectiveCamera(
      CAMERA_FOV,
      ratio,
      CAMERA_NEAR,
      CAMERA_FAR,
    );

    this.camera.position.z = 20;
  }

  addObjects() {
    if (this.debug) {
      this.scene.add(new THREE.AxesHelper(10));
    }
  }

  initRenderer(width, height) {
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(width, height);

    document.body.appendChild(this.renderer.domElement);
  }

  beforeFrameRender() {
    // add some animation logic in inherited class
  }

  mainLoop() {
    this.beforeFrameRender();

    this.renderer.render(this.scene, this.camera);
    requestAnimationFrame(() => this.mainLoop());
  }
}
