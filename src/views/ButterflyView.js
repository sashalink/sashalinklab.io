import * as THREE from 'three';

import { CGA_COLOR } from '../lib/colors'
import BaseView from './BaseView';

// x, y, z
const TAIL = new THREE.Vector3(0, 1, 1);
const HEAD = new THREE.Vector3(4, 2, 1);
const LEFT_WING = new THREE.Vector3(2, 2, -1);
const RIGHT_WING = new THREE.Vector3(2, 2, 4);

const WING_TOP = { x: 1, y: 3 };
const WING_BOTTOM = { x: 2, y: 0 };
const DELTA = 0.05;

const VERTICES = [LEFT_WING, RIGHT_WING, TAIL, HEAD];
const FACES = [
  new THREE.Face3(0, 2, 3),
  new THREE.Face3(1, 2, 3),
];
const MATERIAL = new THREE.MeshBasicMaterial({
  color: CGA_COLOR.MAGENTA,
  side: THREE.DoubleSide,
});

export default class ButterflyView extends BaseView {
  /**
   * @param {object} [params]
   */
  constructor(params = {}) {
    super(params);

    this.shape = undefined;
    this.moveDelta = DELTA;
  }

  setupCamera() {
    super.setupCamera();

    this.camera.position.x = 8;
    this.camera.position.y = 4;
  }

  init() {
    super.init({ background: CGA_COLOR.BLACK });
  }

  addObjects() {
    super.addObjects();

    this.shape = this.factory.createGeometry({
      vertices: VERTICES,
      faces: FACES,
      material: MATERIAL,
    });

    this.shape.rotateY(2);

    this.scene.add(this.shape);
  }

  beforeFrameRender() {
    const [leftWing, rightWing] = this.shape.geometry.vertices;

    this.moveUp = (leftWing.y <= WING_TOP.y) && (leftWing.y >= WING_BOTTOM.y);

    if (leftWing.y >= WING_TOP.y || leftWing.y <= WING_BOTTOM.y) {
      this.moveDelta *= -1;
    }

    leftWing.y += this.moveDelta;
    leftWing.x -= this.moveDelta;
    rightWing.y += this.moveDelta;
    rightWing.x -= this.moveDelta;

    this.shape.geometry.verticesNeedUpdate = true;
  }
}
