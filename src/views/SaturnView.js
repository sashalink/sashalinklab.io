import * as THREE from 'three';
import ShapeFactory from '../lib/ShapeFactory';

const COLOR_BACKGROUND = 0xfafafa;
const COLOR_SPHERE = 0x222222;
const COLOR_RING_INNER = 0x8a8a8a;
const COLOR_RING_MIDDLE = 0x777777;
const COLOR_RING_OUTER = 0x3c3c3c;

const WIDTH = window.innerWidth;
const HEIGHT = window.innerHeight;

const CAMERA_FOV = 75;
const CAMERA_NEAR = 1;
const CAMERA_FAR = 1000;
const CAMERA_RATIO = WIDTH / HEIGHT;

const DISK_SEGMENTS = [2, 16];
const SPHERE_SEGMENTS = [9, 9];

let ADD_Y = 0.01;

export default class SaturnView {
  constructor() {
    this.renderer = undefined;
    this.camera = undefined;
    this.scene = undefined;
    this.shapeFactory = new ShapeFactory();
    this.saturn = undefined;
  }

  init() {
    this.setupScene();
    this.setupCamera();
  
    this.addObjects();
    this.initRenderer();
  }

  setupScene() {
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(COLOR_BACKGROUND);
  }

  setupCamera() {
    this.camera = new THREE.PerspectiveCamera(
      CAMERA_FOV,
      CAMERA_RATIO,
      CAMERA_NEAR,
      CAMERA_FAR,
    );

    this.camera.position.z = 20;
  }

  addObjects() {
    const sphere = this.shapeFactory.createSphere({
      radius: 4, color: COLOR_SPHERE, segments: SPHERE_SEGMENTS,
    });
    const innerRing = this.shapeFactory.createRing({
      radius: 5.1, tube: 0.7, color: COLOR_RING_INNER, segments: DISK_SEGMENTS,
    });
    const middleRing = this.shapeFactory.createRing({
      radius: 6.9, tube: 0.7, color: COLOR_RING_MIDDLE, segments: DISK_SEGMENTS,
    });
    const outerRing = this.shapeFactory.createRing({
      radius: 8.5, tube: 0.7, color: COLOR_RING_OUTER, segments: DISK_SEGMENTS,
    });

    const rings = new THREE.Group();
    rings.add(innerRing, middleRing, outerRing);
    rings.rotation.x = 1.7;
    rings.rotation.y = 0.3;

    this.saturn = new THREE.Group();
    this.saturn.add(sphere, rings);

    this.scene.add(this.saturn);
  }

  initRenderer() {
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(WIDTH, HEIGHT);

    document.body.appendChild(this.renderer.domElement);
  }

  beforeFrameRender() {
    this.camera.position.y += ADD_Y;

    if (this.camera.position.y >= 4 || this.camera.position.y <= -4) {
      ADD_Y *= -1;
    }
  }

  mainLoop() {
    this.beforeFrameRender();

    this.renderer.render(this.scene, this.camera);
    requestAnimationFrame(() => this.mainLoop());
  }
}
