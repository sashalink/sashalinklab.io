import * as THREE from 'three';

import { CGA_COLOR } from '../lib/colors'
import BaseView from './BaseView';

// TorusKnotGeometry
// radius, tupe, tubularSegments, radialSegments, p, q
const TORUS_PARAMS_LARGE = [19, 0.2, 15, 3, 11, 13];
const TORUS_PARAMS_SMALL = [5, 0.035, 27, 3, 11, 13];

export default class ProjectView extends BaseView {
  /**
   * @param {object} [params]
   */
  constructor(params = {}) {
    super(params);

    this.large = undefined;
    this.small = undefined;
    this.axis = undefined;
  }

  setupCamera() {
    super.setupCamera();

    this.camera.position.x = 0;
    this.camera.position.y = 0;
    this.camera.position.z = 2;
  }

  init() {
    super.init({ background: CGA_COLOR.BLACK });
  }

  addObjects() {
    super.addObjects();

    this.large = new THREE.Mesh(
      new THREE.TorusKnotGeometry(...TORUS_PARAMS_LARGE),
      new THREE.MeshBasicMaterial({ color: CGA_COLOR.MAGENTA }),
    );
    
    this.small = new THREE.Mesh(
      new THREE.TorusKnotGeometry(...TORUS_PARAMS_SMALL),
      new THREE.MeshBasicMaterial({ color: CGA_COLOR.LIGHT_CYAN }),
    );

    this.axis = new THREE.Vector3(7, 6, -2);

    this.large.position.z = 12;
    this.small.position.z = -6;

    this.scene.add(this.large);
    this.scene.add(this.small);
  }

  beforeFrameRender() {
    this.large.rotateZ(-0.008);
    this.small.rotateZ(0.001);

    this.camera.rotateOnWorldAxis(this.axis, 0.0001);
  }
}
